exports.rightSession = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJoZWFkZXIiOiJ7IFwiYWxnXCI6IFwiSFMyNTZcIiwgXCJ0eXBcIjogXCJKV1RcIiB9IiwiYm9keSI6eyJpZCI6MSwidXNlcm5hbWUiOiJhbm9ueW1vdXNAZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmIkMTAkYlpmRG9FSTBGOUNkUWtESWZuUlpyZTBoVWtTNS90b2xoL1VxZlE0NGthSldRZ1NwNW1IWEsiLCJjcmVhdGVkQXQiOiIyMDE4LTEwLTAyVDIyOjQ0OjQ5LjAwMFoiLCJ1cGRhdGVkQXQiOiIyMDE4LTEwLTAyVDIyOjQ0OjQ5LjAwMFoifSwiaWF0IjoxNTM4NzIwMzIxfQ.oaA2lB-hTxc-ra-fk0e5PUFvOj0twm93428WHLBJjcQ';
exports.wrongSession = 'dwwfwfwf';

exports.rightBaseUrl = '/api/v1/books';
exports.wrongBaseUrl = '/api/v1/books2';
exports.arrRouteValid =  [
    {
        "id": 1,
        "createdAt": "2018-10-02T22:44:49.000Z",
        "updatedAt": "2018-10-02T22:44:49.000Z",
        "roleId": 1,
        "routeId": 1,
        "route": {
            "id": 1,
            "method": "GET",
            "createdAt": "2018-10-02T22:44:49.000Z",
            "updatedAt": "2018-10-02T22:44:49.000Z",
            "pathId": 3,
            "routePath": {
                "id": 3,
                "path": "books",
                "createdAt": "2018-10-02T22:44:49.000Z",
                "updatedAt": "2018-10-02T22:44:49.000Z",
                "parentId": 2,
                "parent": {
                    "id": 2,
                    "path": "{version}",
                    "createdAt": "2018-10-02T22:44:49.000Z",
                    "updatedAt": "2018-10-02T22:44:49.000Z",
                    "parentId": 6,
                    "parent": {
                        "id": 6,
                        "path": "api",
                        "createdAt": "2018-10-02T22:44:49.000Z",
                        "updatedAt": "2018-10-02T22:44:49.000Z",
                        "parentId": null
                    }
                }
            }
        }
    },
    {
        "id": 5,
        "createdAt": "2018-10-02T22:44:49.000Z",
        "updatedAt": "2018-10-02T22:44:49.000Z",
        "roleId": 1,
        "routeId": 3,
        "route": {
            "id": 3,
            "method": "GET",
            "createdAt": "2018-10-02T22:44:49.000Z",
            "updatedAt": "2018-10-02T22:44:49.000Z",
            "pathId": 4,
            "routePath": {
                "id": 4,
                "path": "authors",
                "createdAt": "2018-10-02T22:44:49.000Z",
                "updatedAt": "2018-10-02T22:44:49.000Z",
                "parentId": 2,
                "parent": {
                    "id": 2,
                    "path": "{version}",
                    "createdAt": "2018-10-02T22:44:49.000Z",
                    "updatedAt": "2018-10-02T22:44:49.000Z",
                    "parentId": 6,
                    "parent": {
                        "id": 6,
                        "path": "api",
                        "createdAt": "2018-10-02T22:44:49.000Z",
                        "updatedAt": "2018-10-02T22:44:49.000Z",
                        "parentId": null
                    }
                }
            }
        }
    }
];