let permissionService = require("../services/permissionService");
let authenticateService = require("../services/authenticateService");

let responseUtil = require('../utils/responseUtil');

let chai = require('chai');
let chaiHttp = require('chai-http');

let expect = require('chai').expect;
let should = require('chai').should();

let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised).should();
chai.use(chaiHttp);

let assert = require('assert');
let bcrypt = require('bcryptjs');

let model = require('../models');
let clientCredentialsModel = model.ClientCredentials;
let userModel = model.User;
let sessionModel = model.Session;

describe('Login: ', () => {
    const email = 'test@gmail.com';
    const password = '123456';

    const clientId = '123456';
    const clientSecret = '654321';
    global.clientSecret = clientSecret;

    const domain = process.env.DOMAIN;

    beforeEach(function(done) {
        this.timeout(5000);

        let filter = {
            where: {},
            truncate: false
        };

        let clientCredentialData = {
            clientName: '',
            clientId: clientId,
            clientSecret: clientSecret
        };

        let userData = {
            username: email,
            password: bcrypt.hashSync(password, 10)
        };

        clientCredentialsModel.destroy(filter).then(() => {
            sessionModel.destroy(filter).then(() => {
                userModel.destroy(filter).then(() => {
                    clientCredentialsModel.create(clientCredentialData).then(() => {
                        userModel.create(userData).then(result => {
                            global.userId = result.dataValues.id;
                            global.session = authenticateService.generateSession(result.dataValues);

                            let sessionData = {
                                userId: global.userId,
                                session: global.session
                            };

                            sessionModel.create(sessionData).then(() => {
                                done();
                            })
                        })
                    })
                })
            })
        })
    });

    describe('/POST /api/v1/login', () => {
        it('Can login with valid username & password', function () {
            let requestData = {
                username: email,
                password: password
            };

            chai.request(domain)
                .post('/api/v1/login')
                .send(requestData)
                .set('clienid', clientId)
                .end((err, res) => {
                    if (err) done();

                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });
});