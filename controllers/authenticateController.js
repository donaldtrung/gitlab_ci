let dotenv = require('dotenv');
let logger = require('../libs/logger');
let responseUtil = require('../utils/responseUtil');
let authenticateService = require('../services/authenticateService');

dotenv.load();

exports.validateRequest = function (dataHeaders) {
    return new Promise(function (resolve, reject) {
        let clientId = dataHeaders.clientid;
        global.language = dataHeaders.accept-language || process.env.DEFAULT_LANGUAGE;

        authenticateService.getClientSecret(clientId).then(function (result) {
            if (!result || !result.dataValues || !result.dataValues.clientSecret) {
                resolve(responseUtil.response(null, responseUtil.FAIL_CLIENT_ID_CODE, responseUtil.FAIL_CLIENT_ID_MESSAGE));
            }

            global.clientSecret = result.dataValues.clientSecret;
            resolve(responseUtil.response(null, responseUtil.SUCCESS_CODE, responseUtil.SUCCESS_MESSAGE));

        }).catch(function (error) {
            reject(error);
        });

    });
};

exports.authenticate = function (request, response, next) {
    let session = request.headers.session;

    if (!session) {
        response.send(responseUtil.response(null, responseUtil.FAIL_AUTHENTICATE_CODE, responseUtil.FAIL_AUTHENTICATE_MESSAGE));
    }

    authenticateService.getSession(session).then(function (result) {

        if(!result) {
            return response.send(responseUtil.response(null, responseUtil.FAIL_AUTHENTICATE_CODE, responseUtil.FAIL_AUTHENTICATE_MESSAGE));
        }

        next();

    }).catch(function (error) {
        logger.logError(error.message);
        response.send(responseUtil.response(null, responseUtil.FAIL_AUTHENTICATE_CODE, error.message));
    })
};

exports.login = function (request, response) {
    let {username, password} = request.body;

    authenticateService.checkLogin(username, password).then(function (user) {

        if(!user) {
            let responseMessage = responseUtil.response(null, responseUtil.FAIL_CODE, responseUtil.FAIL_LOGIN_MESSAGE);
            return response.send(responseMessage);
        }

        authenticateService.handleSession(user.dataValues).then(function (session) {

            let dataUser = {
                id: user.dataValues.id,
                username: user.dataValues.username,

                role: user.dataValues.role,
                session: session,

                createdAt: user.dataValues.createdAt,
                updatedAt: user.dataValues.updatedAt
            };

            let responseMessage = responseUtil.response(dataUser, responseUtil.SUCCESS_CODE, responseUtil.SUCCESS_MESSAGE);
            return response.send(responseMessage);

        }).catch(function (error) {
            logger.logError(error.message);
            let responseMessage = responseUtil.response(null, responseUtil.FAIL_CODE, error.message);
            return response.send(responseMessage);
        });

    }).catch(function (error) {
        let responseMessage = responseUtil.response(null, responseUtil.FAIL_CODE, error.message);
        response.send(responseMessage);
    });
};

exports.logout = function (request, response) {
    let session = request.headers.session;
    
    authenticateService.removeSession(session).then(function () {
        let responseMessage = responseUtil.response(null, responseUtil.SUCCESS_CODE, responseUtil.SUCCESS_MESSAGE);
        response.send(responseMessage);

    }).catch(function (error) {
        logger.logError(error.message);
        let responseMessage = responseUtil.response(null, responseUtil.FAIL_CODE, error.message);
        response.send(responseMessage);
    });
};