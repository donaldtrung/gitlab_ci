'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Routes', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },

            pathId: // name of the key we're adding
                {
                    type: Sequelize.INTEGER,
                    foreignKey: true,
                    references: {
                        model: 'RoutePaths', // name of Target model
                        key: 'id' // key in Target model that we're referencing
                    },
                    onUpdate: 'CASCADE',
                    onDelete: 'CASCADE'
                },

            method: {
                type: Sequelize.STRING
            },

            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Routes');
    }
};