'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('RoleRoutes', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },

            routeId: // name of the key we're adding
                {
                    type: Sequelize.INTEGER,
                    foreignKey: true,
                    references: {
                        model: 'Routes', // name of Target model
                        key: 'id' // key in Target model that we're referencing
                    },
                    onUpdate: 'CASCADE',
                    onDelete: 'CASCADE'
                },

            roleId: // name of the key we're adding
                {
                    type: Sequelize.INTEGER,
                    foreignKey: true,
                    references: {
                        model: 'Roles', // name of Target model
                        key: 'id' // key in Target model that we're referencing
                    },
                    onUpdate: 'CASCADE',
                    onDelete: 'CASCADE'
                },

            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('RoleRoutes');
    }
};