var winston = require('winston'),
    dotenv = require('dotenv'),
    dateTime = require('node-datetime'),
    fs = require('fs');

//Load .env
dotenv.load();

var logger = new winston.Logger({
    level: 'info',
    json: false,
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            filename: getFilename('public/logs/'),
            json: false
        })
    ]
});

var jsonLogger = new winston.Logger({
    level: 'info',
    json: true,
    timestamp: false,
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            filename: process.env.JSON_LOGFILE ? process.env.JSON_LOGFILE : 'logs/json_systems.log',
            // handleExceptions: true,
            json: true
        })
    ]
});

function getFilename(prefix) {
    let path = prefix + rotateName();
    if (!fs.existsSync(path)) {
        let createStream = fs.createWriteStream(path);
        createStream.end();
    }

    return path;
}

function rotateName() {
    let dt = dateTime.create();
    let formatted = dt.format('d-m-Y');
    return formatted + '.log'
}

exports.dump = function(obj) {
    console.log(obj);
};

exports.logError = function(message, obj, error) {
    logger.error(message, obj, error);
};

exports.logInfo = function(message, obj) {
    let debugMod = process.env.DEBUG;
    console.log('Debug mode.... ', debugMod);
    logger.info(message, obj);
};

exports.logInfoJS = function(message, obj) {
    jsonLogger.info(message, obj);
};
