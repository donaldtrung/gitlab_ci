const axios = require('axios');
let dotenv = require('dotenv');
dotenv.load();

exports.requestHttp = function (method, url, data) {
    let header = {
        clientid: process.env.CLIENT_ID || null,
        session: global.user ? global.user.session : null
    };

    if(global.user)
    console.log(global.user.session);
    return new Promise(function (resolve, reject) {
        axios({method: method, url: url, headers: header, data: data}).then(function (response) {
            resolve(response);
        }).catch(function (error) {
            reject(error);
        });
    });
};