exports.SUCCESS_CODE = 200;
exports.SUCCESS_MESSAGE = 'Success';

exports.FAIL_CODE = 400;
exports.FAIL_MESSAGE = 'System Error';
exports.INVALID_REQUEST_MESSAGE = 'Invalid Request';

exports.FAIL_CLIENT_ID_CODE = 301;
exports.FAIL_CLIENT_ID_MESSAGE = 'Client id not found';

exports.FAIL_LOGIN_MESSAGE = 'Username & password do not match';

exports.FAIL_AUTHENTICATE_CODE = 401;
exports.FAIL_AUTHENTICATE_MESSAGE = 'Permission Denied';

exports.INVALID_SESSION_MESSAGE = 'Session Invalid';

exports.response = function (data, code, message) {
    let resObject = {
        status: {
            code: code,
            message: message
        }
    };

    if (data !== null) {
        resObject['data'] = data;
    }

    return resObject;
};