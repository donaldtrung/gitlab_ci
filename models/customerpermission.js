'use strict';
module.exports = (sequelize, DataTypes) => {
    const CustomerPermission = sequelize.define('CustomerPermission', {
        name: DataTypes.STRING,
        conditions: DataTypes.STRING
    }, {});
    CustomerPermission.associate = function (models) {
        // associations can be defined here
        models.CustomerPermission.belongsTo(models.Permission, {foreignKey: 'permissionId', targetKey: 'id'});
    };
    return CustomerPermission;
};