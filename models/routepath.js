'use strict';
module.exports = (sequelize, DataTypes) => {
    const RoutePath = sequelize.define('RoutePath', {
        path: DataTypes.STRING
    }, {});
    // RoutePath.isHierarchy();
    RoutePath.associate = function (models) {
        // models.RoutePath.belongsToMany(models.RoutePath, {as: 'parentId', through: 'id'});
        models.RoutePath.hasMany(models.Route, {as: 'routePath', foreignKey: 'pathId'});

        // models.RoutePath.hasMany( models.RoutePath, { as: 'child', foreignKey: 'parentId', hierarchy: true  } );
        // models.RoutePath.belongsTo( models.RoutePath, { as: 'parent', foreignKey: 'parentId', hierarchy: true  } );
    };
    return RoutePath;
};