'use strict';
module.exports = (sequelize, DataTypes) => {
    const Session = sequelize.define('Session', {
        session: DataTypes.STRING
    }, {});
    Session.associate = function (models) {
        // associations can be defined here
        models.Session.belongsTo(models.User, {foreignKey: 'userId', targetKey: 'id'});
    };
    return Session;
};