'use strict';
module.exports = (sequelize, DataTypes) => {
    const RoleRoute = sequelize.define('RoleRoute', {}, {});
    RoleRoute.associate = function (models) {
        models.RoleRoute.belongsTo(models.Role, {as: 'roleRoute', foreignKey: 'roleId', targetKey: 'id'});
        models.RoleRoute.belongsTo(models.Route, {as: 'route', foreignKey: 'routeId', targetKey: 'id'});
    };

    return RoleRoute;
};