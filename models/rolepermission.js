'use strict';
module.exports = (sequelize, DataTypes) => {
    const RolePermission = sequelize.define('RolePermission', {}, {});
    RolePermission.associate = function (models) {
        // associations can be defined here
        models.RolePermission.belongsTo(models.Permission, {foreignKey: 'permissionId', targetKey: 'id'});
        models.RolePermission.belongsTo(models.Role, {foreignKey: 'roleId', targetKey: 'id'});
    };
    return RolePermission;
};