'use strict';
module.exports = (sequelize, DataTypes) => {
    const ClientCredentials = sequelize.define('ClientCredentials', {
        clientId: DataTypes.STRING,
        clientName: DataTypes.STRING,
        clientSecret: DataTypes.STRING
    }, {});
    ClientCredentials.associate = function (models) {
        // associations can be defined here
    };
    return ClientCredentials;
};