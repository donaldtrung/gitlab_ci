'use strict';
module.exports = (sequelize, DataTypes) => {
    const UserRole = sequelize.define('UserRole', {}, {});
    UserRole.associate = function (models) {
        models.UserRole.belongsTo(models.Role, {as: 'role',foreignKey: 'roleId', targetKey: 'id'});
        models.UserRole.belongsTo(models.User, {foreignKey: 'userId', targetKey: 'id'});
    };
    return UserRole;
};