'use strict';
module.exports = (sequelize, DataTypes) => {
    const Route = sequelize.define('Route', {
        method: DataTypes.STRING
    }, {});
    Route.associate = function (models) {
        models.Route.hasMany(models.RoleRoute, {as: 'route', foreignKey: 'routeId'});
        models.Route.belongsTo(models.RoutePath, {as: 'routePath', foreignKey: 'pathId', targetKey: 'id'});
    };
    return Route;
};