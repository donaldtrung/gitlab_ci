'use strict';
module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define('Role', {
        name: DataTypes.STRING
    }, {});
    Role.associate = function (models) {
        models.Role.hasMany(models.UserRole, { as: 'role', foreignKey: 'roleId'});
        models.Role.hasMany(models.RoleRoute, {as: 'roleRoute', foreignKey: 'roleId'});
        // associations can be defined here
    };
    return Role;
};