let dotenv = require('dotenv');
let responseUtil = require("../utils/responseUtil");
let authenticateController = require('../controllers/authenticateController');

dotenv.load();
let appRouter = function (app, express) {
    let router = express.Router();

    router.use(function (request, response, next) {
        authenticateController.validateRequest(request.headers).then(function (result) {
            if (result.status.code !== responseUtil.SUCCESS_CODE) {
                let responseMessage = responseUtil.response(null, result.status.code, result.status.message);
                return response.status(responseUtil.FAIL_CLIENT_ID_CODE).send(responseMessage);
            }

            next();
        }).catch(function (error) {
            let responseMessage = responseUtil.response(null, responseUtil.FAIL_CODE, error.message);
            response.status(responseUtil.FAIL_CLIENT_ID_CODE).send(responseMessage);
        });
    });

    router.post('/login', function (request, response) {
        authenticateController.login(request, response);
    });

    router.get('/access', authenticateController.authenticate, function (request, response) {
        response.send("Access Successfull");
    });

    router.get('/logout', authenticateController.authenticate, function (request, response) {
        authenticateController.logout(request, response);
    });

    app.use(process.env.DEFAULT_REFIX_URL, router);
};


module.exports = appRouter;



