let dotenv = require('dotenv');
let axiosLib = require('../libs/axios');
let responseUtil = require('../utils/responseUtil');
let Handlebars = require('handlebars');

let bcrypt = require('bcryptjs');
let model = require('../models');

let userModel = model.User;
let roleModel = model.Role;
let userRoleModel = model.UserRole;
let roleRouteModel = model.RoleRoute;
let routePathModel = model.RoutePath;
let routeModel = model.Route;

let authenticateService = require("../services/authenticateService");
dotenv.load();

let appRouter = function (app, express) {
    let router = express.Router();

    router.get('/', function (request, response) {
        response.render('login');
    });

    router.get('/demo/register', function (request, response) {
        response.render('register');
    });

    router.post('/demo/register', function (request, response) {

        let data = {
            username: request.body.username,
            password: bcrypt.hashSync(request.body.password, 10)
        };

        userModel.create(data).then(function () {
            response.redirect('/home');
        }).catch(function () {
            response.redirect('/demo/register');
        });
    });

    router.post('/demo/login', function (request, response) {
        let data = {
            username: request.body.username,
            password: request.body.password
        };

        axiosLib.requestHttp('post', process.env.DOMAIN + '/api/v1/login', data).then(function (res) {
            if (res.data.status.code == responseUtil.SUCCESS_CODE) {
                global.user = res.data.data;
                response.redirect('/home');
            } else {
                response.redirect('/');
            }
        }).catch(function (res) {
            console.log(res.message);
        })
    });

    router.get('/demo/generate-key', function (request, response) {
        authenticateService.generationAuthencationKey('random').then(function (result) {
            response.send(result)
        }).catch(function (error) {
            response.send('Random fail: ' + error.message);
        });
    });


    router.use(function (request, response, next) {
        if (!global.user) {
            return response.redirect('/');
        }

        next();
    });

    router.get('/demo/logout', function (request, response) {
        global.user = null;

        axiosLib.requestHttp('post', process.env.DOMAIN + '/api/v1/logout', []).then(function () {
            response.redirect('/');
        }).catch(function (res) {
            console.log(res.message);
        })

        response.redirect('/');
    });

    router.get('/home', function (request, response) {
        let filterUserRole = {
            where: {
                userId: global.user.id
            },
            attributes: ['roleId']
        };

        let filterRole = {
            include: [{
                model: model.RoutePath,
                as: 'routePath'
            }]
        };

        Promise.all([
            roleModel.findAll(),
            userRoleModel.findAll(filterUserRole),
            routeModel.findAll(filterRole)
        ]).then(function (data) {
            response.render('home', {
                roles: data[0],
                userRole: data[1],
                routes: data[2]
            });
        }).catch(function () {
            response.redirect('/');
        });
    });

    router.get('/demo/http-request', function (request, response) {
        axiosLib.requestHttp(request.query.method, process.env.DOMAIN + request.query.path, []).then(function (res) {
            response.send(res.data);
        }).catch(function (res) {
            response.send(res.message);
        })
    });

    router.get('/demo/post-books', function (request, response) {
        axiosLib.requestHttp('post', process.env.DOMAIN + '/api/v1/books', []).then(function (res) {
            response.send(res.data);
        }).catch(function (res) {
            response.send(res.message);
        })
    });

    router.get('/roles/:roleId/manage', function (request, response) {
        let filter = {
            where: {
                roleId: request.params.roleId,
            },

            include: [{
                model: model.Route,
                as: 'route',

                include: [{
                    model: model.RoutePath,
                    as: 'routePath'
                }]
            }]
        };

        Promise.all([
            roleRouteModel.findAll(filter),
            routePathModel.findAll()
        ]).then(function (data) {
            response.render('role_detail', {
                routePaths: data[0],
                roleId: request.params.roleId,
                paths: data[1]
            });
        }).catch(function (error) {
            response.send(error.message);
        });
    });

    router.post('/roles/:roleId/manage', async function (request, response) {
        let dataRoute = {
            method: request.body.method,
            pathId: request.body.pathId
        };

        let route = await routeModel.findOne({where: dataRoute});

        if (!route) {
            route = await routeModel.create(dataRoute);
        }

        if (!route) {
            response.redirect('/roles/' + request.params.roleId + '/manage');
        }

        let dataRoleRoute = {
            routeId: route.dataValues.id,
            roleId: Number(request.params.roleId)
        };

        let roleRoute = await roleRouteModel.findOne({where: dataRoleRoute});

        if (!roleRoute) {
            await roleRouteModel.create(dataRoleRoute);
        }

        response.redirect('/roles/' + request.params.roleId + '/manage');
    });

    router.delete('/roles/:roleId/manage', function (request, response) {
        let criteriaRoute = {
            where: {
                id: request.body.id
            }
        };

        routeModel.destroy(criteriaRoute).then(function (result) {
            let responseMessage = responseUtil.response(null, responseUtil.SUCCESS_CODE, responseUtil.SUCCESS_MESSAGE);
            response.send(responseMessage);
        }).catch(function (error) {
            let responseMessage = responseUtil.response(null, responseUtil.FAIL_CODE, responseUtil.FAIL_MESSAGE);
            response.send(responseMessage);
        });
    });

    router.get('/roles/manage', function (request, response) {
        roleModel.findAll().then(function (result) {
            response.render('role', {
                roles: result
            });

        }).catch(function (error) {
            response.send(error.message);
        });
    });

    router.post('/roles/manage', function (request, response) {
        if (!request.body.role) {
            return response.redirect('/roles/manage');
        }

        let data = {
            name: request.body.role
        };

        roleModel.create(data).then(function (result) {
            response.redirect('/roles/manage');
        }).catch(function (error) {
            response.redirect('/roles/manage');
        });
    });

    router.delete('/roles/manage', function (request, response) {
        let criteria = {
            where: {
                id: request.body.id
            }
        };

        roleModel.destroy(criteria).then(function (result) {
            let responseMessage = responseUtil.response(null, responseUtil.SUCCESS_CODE, responseUtil.SUCCESS_MESSAGE);
            response.send(responseMessage);
        }).catch(function (error) {
            let responseMessage = responseUtil.response(null, responseUtil.FAIL_CODE, responseUtil.FAIL_MESSAGE);
            response.send(responseMessage);
        });
    });

    router.get('/paths/manage', function (request, response) {
        routePathModel.findAll().then(function (result) {
            response.render('path', {
                paths: result
            });

        }).catch(function (error) {
            response.send(error.message);
        });
    });

    router.post('/paths/manage', function (request, response) {
        if (!request.body.path) {
            return response.redirect('/paths/manage');
        }

        let data = {
            path: request.body.path
        };

        routePathModel.create(data).then(function (result) {
            response.redirect('/paths/manage');
        }).catch(function (error) {
            response.redirect('/paths/manage');
        });
    });

    router.delete('/paths/manage', function (request, response) {
        let criteria = {
            where: {
                id: request.body.id
            }
        };

        routePathModel.destroy(criteria).then(function (result) {
            let responseMessage = responseUtil.response(null, responseUtil.SUCCESS_CODE, responseUtil.SUCCESS_MESSAGE);
            response.send(responseMessage);
        }).catch(function (error) {
            let responseMessage = responseUtil.response(null, responseUtil.FAIL_CODE, responseUtil.FAIL_MESSAGE);
            response.send(responseMessage);
        });
    });

    router.get('/roles/user', function (request, response) {
    });

    router.post('/roles/user', function (request, response) {
        let roles = request.body.checkboxRole;
        let filter = {
            where: {
                userId: global.user.id
            }
        };

        userRoleModel.destroy(filter).then(function () {
            for (let index = 0; index < roles.length; index++) {
                let data = {
                    userId: global.user.id,
                    roleId: roles[index]
                };

                userRoleModel.create(data).then(function (result) {
                    if (index == roles.length - 1) {
                        response.redirect('/home');
                    }
                });
            }
        }).catch(function (error) {
            console.log(error.message);
            response.redirect('/home');
        })
    });

    app.use('/', router);
};

module.exports = appRouter;


Handlebars.registerHelper('global', function (key) {
    return eval("global." + key);
});

var handlebars = require('handlebars');
var layouts = require('handlebars-layouts');
var fs = require('fs');
// Register helpers
handlebars.registerHelper(layouts(handlebars));

// Register partials
handlebars.registerPartial('layout', fs.readFileSync(global.rootDir + '/views/layouts/master_layout.handlebars', 'utf8'));
handlebars.registerPartial('layout-admin', fs.readFileSync(global.rootDir + '/views/template/master_layout.handlebars', 'utf8'));
handlebars.registerPartial('layout-admin-sidebar', fs.readFileSync(global.rootDir + '/views/template/partials/sidebar.handlebars', 'utf8'));


Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {

    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '!=':
            return (v1 != v2) ? options.fn(this) : options.inverse(this);
        case '!==':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        case 'existsRoleId':
            for (let index = 0; index < v2.length; index++) {
                if (v2[index].dataValues.roleId == v1) {
                    return options.fn(this);
                }
            }

            return options.inverse(this);
        default:
            return options.inverse(this);
    }
});

Handlebars.registerHelper('fullUrl', function (path) {
    return process.env.DOMAIN + path;
});



