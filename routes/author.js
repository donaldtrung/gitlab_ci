let dotenv = require('dotenv');
let permissionController = require('../controllers/permissionController');

dotenv.load();

let appRouter = function (app, express) {
    let router = express.Router();

    router.get('/', permissionController.validatePermission, function (request, response) {
        response.send("Hey you have permission access GET AUTHORS");
    });

    app.use(process.env.DEFAULT_REFIX_URL + '/authors', router);
};

module.exports = appRouter;



