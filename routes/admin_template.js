let dotenv = require('dotenv');
let permissionController = require('../controllers/permissionController');

dotenv.load();

let appRouter = function (app, express) {
    let router = express.Router();

    router.get('/', function (request, response) {
        response.render('template/index', {
            // routePaths: data[0],
            // roleId: request.params.roleId,
            // paths: data[1]
        });
    });

    app.use('/admin/template', router);
};

module.exports = appRouter;



