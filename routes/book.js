let dotenv = require('dotenv');
let responseUtil = require("../utils/responseUtil");
let permissionController = require('../controllers/permissionController');

dotenv.load();

let appRouter = function (app, express) {
    let router = express.Router();

    router.get('/', permissionController.validatePermission, function (request, response) {
        response.send("Hey you have permission access GET BOOK");
    });

    router.post('/', permissionController.validatePermission, function (request, response) {
        response.send("Hey you have permission access POST BOOK");
    });

    router.get('/favourite', permissionController.validatePermission, function (request, response) {
        response.send("Hey you have permission access GET favourite BOOK");
    });

    app.use(process.env.DEFAULT_REFIX_URL + '/books', router);
};


module.exports = appRouter;



