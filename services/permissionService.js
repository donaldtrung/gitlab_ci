let bcrypt = require('bcryptjs');
let crypto = require('crypto');

let model = require('../models');
let jwt = require('jsonwebtoken');
let responseUtil = require('../utils/responseUtil');

let userRoleModel = model.UserRole;

exports.isValidSession = function (session) {
    return new Promise(function (resolve, reject) {
        let dataJWT = jwt.verify(session, global.clientSecret);

        if (!dataJWT || !dataJWT.body.id) {
            reject({message: responseUtil.INVALID_SESSION_MESSAGE});
        }

        resolve(dataJWT.body.id);
    });
};

exports.getRouteData = function (method, userId) {
    return new Promise(function (resolve, reject) {
        let filter = {
            where: {
                userId: userId,
            },

            include: [{
                model: model.Role,
                as: 'role',
                include: [{
                    model: model.RoleRoute,
                    as: 'roleRoute',
                    include: [{
                        model: model.Route,
                        as: 'route',
                        where: {
                            method: method
                        },
                        include: [{
                            model: model.RoutePath,
                            as: 'routePath',
                            // include: [
                            //     {
                            //         model: model.RoutePath,
                            //         as: 'parent',
                            //         include: [{
                            //             model: model.RoutePath,
                            //             as: 'parent',
                            //         }]
                            //     }
                            // ]
                        }]
                    }]
                }]
            }]
        };

        userRoleModel.findAll(filter).then(function (result) {
            resolve(result);
        }).catch(function (error) {
            reject(error);
        });
    });
};

exports.checkRoutePermission = function (arrRouteUrl, arrRouteValid) {
    for (let index = 0; index < arrRouteValid.length; index++) {
        let routePath = arrRouteValid[index].route.routePath;

        if (!routePath) {
            continue;
        }

        if (isExistedRoute(arrRouteUrl, routePath)) {
            return true;
        }
    }

    return false;
};

exports.checkRoutePermissionV2 = function (arrRouteUrl, arrRouteValid) {
    for (let index = 0; index < arrRouteValid.length; index++) {
        let routePath = arrRouteValid[index].route.routePath;

        if (!routePath) {
            continue;
        }

        if (isExistedRouteV2(arrRouteUrl, routePath)) {
            return true;
        }
    }

    return false;
};

function isExistedRouteV2(arrRouteUrl, routePath) {
    if (!routePath || !routePath.path) {
        return false;
    }

    let path = routePath.path;
    let arrPathValid = path.split("/");

    for (let index = arrPathValid.length - 1; index >= 0; index--) {
        if (isDynamicParameter(arrPathValid[index])) {
            continue;
        }

        if (arrRouteUrl[index] != arrPathValid[index]) {
            return false;
        }
    }

    return true;
};

function isExistedRoute(arrRouteUrl, routePath) {
    for (let index = arrRouteUrl.length - 1; index >= 0; index--) {
        if (!routePath) {
            return true;
        }

        if (isDynamicParameter(routePath.path)) {
            routePath = routePath.parent;
            continue;
        }

        if (arrRouteUrl[index] && arrRouteUrl[index] == routePath.path) {
            routePath = routePath.parent;
        } else {
            return false;
        }
    }
};

function isDynamicParameter(param) {
    return param.indexOf("{") > -1 && param.indexOf("}") > -1;
}