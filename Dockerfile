FROM node:8

RUN mkdir /src
WORKDIR /src

COPY . /src
RUN npm install --silent

#RUN touch /src/logs/systems.log

#ENV NODE_ENV test
ENV DOMAIN https://permissionpro.herokuapp.com

ENV DEFAULT_LANGUAGE en
ENV DEFAULT_USER_ROLE 1
ENV DEFAULT_HEADER_JWT { "alg": "HS256", "typ": "JWT" }
ENV DEFAULT_REFIX_URL /api/:version
ENV CLIENT_ID 6ca86db265d78cb6493881e3555904f2f1a322a2d8fe8ac0db1a171dd3bf9a65
ENV NODE_ENV production

EXPOSE 80:3000
CMD [ "npm", "start" ]
