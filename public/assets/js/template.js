TemplateJS = {
    deleteHandler: function () {
        $('.delete-btn').click(function () {
            if (confirm('Are you sure you want to delete this?')) {
                let url = $('.ajax-url').val();
                let data = {
                    id: $(this).attr("value")
                };

                $.ajax({
                    type: "DELETE",
                    url: url,
                    data: data,
                    success: function (result) {
                        if (result && result.status.code == 200) {
                            location.reload();
                        }
                    }
                });
            }
        });
    },

    init: function () {
        TemplateJS.deleteHandler();
    }
};


$(function () {
    TemplateJS.init();
});
